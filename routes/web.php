<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/dashbord', function () {
//    return view('admin.content');
//})->name("dash");
Auth::routes();
//Route::group(['middlewareGroups' => 'web'], function () {
  /* // Route::get('local/{local}', function ($lang) {
        if (in_array($lang, ["ar", "en"])) {
            //   Session::put('local', $lang);

//Illuminate\Support\Facades\App::setLocale($lang);
            session()->put('local', $lang);

            //return response(session()->all());

        }
        return session()->get('local');

        // return response(app()->getLocale());
        // return  view('admin.products.index');

    })->name('lang');
});*/

Route::group(['prefix' => 'dashboard','namespace'=>'admin'], function () {
Route::get('set-locale/{locale}', function ($lang) {
    if (array_key_exists($lang, \Config::get('language'))) {
        \Session::put('locale', $lang);
    }
    return back();
})->name('lang');
});

Route::group(['prefix' => 'dashboard','namespace'=>'admin',"middleware"=>['auth',"permission"]], function () {
    Route::get('/', function () {
        return view('admin.content');
    })->name("dash");
    Route::post("/products/filter", "ProductsController@filter")->name("filter");
    Route::get("/products/{product}/show", "CategoriesController@products");
    route::resource('users', 'UsersController');
    route::resource('categories', 'CategoriesController');
    route::resource('logs', 'LogsController');
    route::resource('products', 'ProductsController');
    Route::get('/products/ProductDiscount', function () {
        return view ('admin.products.ProductDiscount');

    });

});

Route::get("/app",function(){

    return  response( Route::currentRouteName());
})->name("pppppp");


    Route::get('/products/ProductDiscount',"admin\ProductsController@DiscountCreate" );

route::resource('carts', 'cart');


Route::post('/products/DiscoutStore', 'admin\ProductsController@DiscountStore')->name('products.StoreDiscount');

Route::get('/products/TagsCreate',"admin\ProductsController@Tags_Create");
Route::post('/products/TagsStore', 'admin\ProductsController@Tags_Store')->name('products.TagsStore');

Route::get('/products',"cart@view_products")->name('products');
Route::get('/add-to-cart/{id}',"cart@AddToCart");
Route::get("/pay","PaypalController@payWithpaypal")->name('payment');
//Route::get('/cart',"cart@index");

Route::get('/',function(){

    return "ERROR";
})->name('error');

//Route::delete('remove-from-cart', 'ProductsController@remove');
/*Route::get('/home', function(){

    return view("admin.layout.master");
})->name('home')*/
