@extends('admin.layout.master')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">



                <h4 class="header-title m-t-0 m-b-30">Input Types</h4>

                <div class="row">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if (session()->has('success'))
                        <div class="alert alert-success">
                            <h4>{{session()->get('success')}}</h4>
                        </div>
                    @endif

                    <div class="col-lg-6">
                        <form class="form-horizontal" role="form" method="post" action="{{route('products.StoreDiscount')}}" enctype="multipart/form-data">
                            {{csrf_field()}}


                            <div class="form-group">
                                <label class="col-md-2 control-label">{{__("app.product")}}</label>
                                <div class="col-md-10">
                                    <select name="product_id" class="form-control">
                                        @foreach($products as $key => $value)
                                            <option  name="product_id" value="{{ $key }}">
                                                {{ $value }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                             <div class="form-group">
                                <label class="col-md-2 control-label">{{__("app.product_discount_name")}}</label>
                                <div class="col-md-10">
                                    <input type="text" name="name" class="form-control" >
                                </div>
                            </div>
                              <div class="form-group">
                                <label class="col-md-2 control-label">{{__("app.product_discount_value")}}</label>
                                <div class="col-md-10">
                                    <input type="text" name="value" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">{{__("app.expired_date")}}</label>
                                <div class="col-md-10">
                                    <input type="date" name="expired_date" class="form-control" >
                                </div>
                            </div>





                            <div class="form-group">

                                <div class="col-md-10">
                                    <button type="submit" class="btn btn-primary">{{__("app.save")}}</button>
                                </div>
                            </div>

                        </form>
                    </div><!-- end col -->



                </div><!-- end row -->
            </div>
        </div><!-- end col -->
    </div>
    <!-- end row -->


@endsection
