@extends('admin.layout.master')

@section('styles')
@endsection


@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">
                <div class="dropdown pull-right">
                    <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                        <i class="zmdi zmdi-more-vert"></i>
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                    </ul>
                </div>

                <h4 class="header-title m-t-0 m-b-30">products</h4>
                <a href="/dashboard/products/create" class="btn btn-primary">create</a>
                <form method="post" action="{{route("filter")}}" class="form-control">
                    {{csrf_field()}}
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2> pick date</h2>
                            </div>
                            <div class="body">

                                <div class="col-sm-6 col-xs-6">
                                    <div class="form-group form-float">
                                        <label class="form-label">to</label>
                                        <div class="form-line">
                                          <input type="date"  name="to"  class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-6">
                                    <div class="form-group form-float">
                                        <label class="form-label">from</label>
                                        <div class="form-line">
                                          <input type="date"  name="from" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <input type="submit" value="search" class="btn btn-success">
                            </div>
                        </div>
                    </div>
                </div>
                </form>
                <table id="datatable-buttons" class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        @lang("en")
                        <th>{{__("app.name_en")}}</th>
                        @endlang
                        @lang("ar")
                        <th>{{__("app.name_ar")}}</th>
                        @endlang

                        <th>{{__("app.product_des")}}</th>
                        <th>{{__("app.product_price")}}</th>
                        <th>{{__("app.category")}}</th>
                        <th>{{__("app.image")}}</th>
                    </tr>
                    </thead>

                    <tbody>
                    @forelse($products as $product)
                        <tr>
                            <td>{{$product->name()}}</td>
                            <td>{{$product->description}}</td>
                            <td>{{$product->price}}</td>
                            <td>{{$product->category->name()}}</td>
                            <td>
                                <img src="/images/{{$product->image}}" style="width: 100px; height: 100px; border-radius: 50%;">
                            </td>
                            <td>

                                @if(Illuminate\Support\Facades\Auth::user())
                                <form action="{{ route('products.destroy',$product->id) }}" method="POST">


                                    <a class="btn btn-primary" href="{{ route('products.destroy',$product->id) }}">{{__("app.delete")}}</a>

                                    @csrf
                                    @method('DELETE')

                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </form> </td>
<td>

                                <a href="/dashboard/products/{{$product->id}}/show" class="btn btn-primary">{{__("app.products")}}</a>
</td>

                            <td>                                <a href="/dashboard/products/{{$product->id}}/AssignTags" class="btn btn-primary">{{__("app.assign_tags")}}</a>

                                @endif
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td></td>
                            <td></td>
                            <td> </td>
                            <td> </td>
                            <td></td>
                        </tr>
                    @endforelse

                    </tbody>
                </table>
            </div>
        </div><!-- end col -->
    </div>
    <!-- end row -->

@endsection



@section('scripts')
@endsection
