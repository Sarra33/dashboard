                            <div class="form-group">
                                <label class="col-md-2 control-label">{{__("app.name_ar")}}</label>

                                <div class="col-md-10">
                                    {!! Form::text('name_ar',null,["class"=>'form-control']) !!}


                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">{{__("app.name_en")}}</label>
                                <div class="col-md-10">

                                    {!! Form::text('name_en',null,["class"=>'form-control']) !!}

                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" >{{__('app.product_des')}}</label>
                                <div class="col-md-10">
                                    {!! Form::text('description_en',null,['class'=>'form-control"'])  !!}

                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">{{__("app.product_price")}}</label>
                                <div class="col-md-10">
                                    {!! Form::text('price',null,['class'=>'form-control"']) !!}

                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">{{__("app.product_cat")}}</label>
                                <div class="col-md-10">
                                    {!! Form::select("category_id",$category,null,['class'=>'form-control']) !!}



                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">{{__("app.product_expire_date")}}</label>
                                <div class="col-md-10">
                                  {!! form::date('expired_date',null,['class'=>'form-control']) !!}


                                </div>
                            </div>
                            <div class="form-group">

                                <div class="col-md-10">
                                    <label class="col-md-2 control-label">{{__("app.image")}}</label>
                                    {!! Form::file('image',null,["class"=>'btn btn-primary']) !!}

                                </div>
                            </div>