@extends('admin.layout.master')

@section('styles')
@endsection


@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">
                <div class="dropdown pull-right">
                    <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                        <i class="zmdi zmdi-more-vert"></i>
                    </a>

                </div>

                <h4 class="header-title m-t-0 m-b-30">Buttons Example</h4>

                <table id="datatable-buttons" class="table table-striped table-bordered">
                    <thead>
                    <tr>
                       @lang("en")
                        <th>{{__("app.name_en")}}</th>
                        @endlang
                        @lang("ar")
                        <th>{{__("app.name_ar")}}</th>
                        @endlang



                        <th>{{__("app.image")}}</th>
                        <th>{{__("app.products_no")}}</th>
                        
                    </tr>
                    </thead>

                    <tbody>
                    @forelse($categories as $category)
                    <tr>
<td>{{$category->name()}} </td>
                        <td>
                            <img src="/images/{{$category->image}}" style="width: 100px; height: 100px; border-radius: 50%;">
                        </td>
                        <td>{{$category->products->count()}} </td>



                        <td>
                            <a href="/dashboard/categories/{{$category->id}}/edit" class="btn btn-primary">{{__("app.edit")}}</a>

                          <form action="{{ route('categories.destroy',$category->id) }}" method="POST">


                           {{-- <a class="btn btn-primary" href="{{ route('categories.destroy',$category->id) }}">delete</a>--}}

                            @csrf
                            @method('DELETE')

                            <button type="submit" class="btn btn-danger">Delete</button>
                            </form>

                             <a href="/dashboard/products/{{$category->id}}/show" class="btn btn-primary">products</a>
                    </tr>
                        @empty
                        <tr>
                            <td></td>
                            <td></td>
                            <td> </td>
                            <td> </td>
                            <td></td>
                        </tr>
                    @endforelse

                    </tbody>
                </table>
            </div>
        </div><!-- end col -->
    </div>
    <!-- end row -->

@endsection



@section('scripts')
@endsection
