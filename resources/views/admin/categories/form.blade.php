
<div class="form-group">
    <label class="col-md-2 control-label">{{__('app.name_en')}}</label>
    <div class="col-md-10">
        {!! Form::text("name_en",null,["class"=>"form-control"]) !!}

    </div>
</div>
<div class="form-group">
    <label class="col-md-2 control-label">{{__('app.name_ar')}}</label>
    <div class="col-md-10">

        {!! Form::text("name_ar",null,["class"=>"form-control"]) !!}
    </div>
</div>

<div class="form-group">
    <label class="col-md-2 control-label">{{__('app.category_image')}}</label>
    <div class="col-md-10">
        {!! Form::file("image",null,["class"=>"form-control"]) !!}
    </div>
</div>
<div class="form-group">
    <label class="col-md-2 control-label">{{__('app.expired_date')}}</label>
    <div class="col-md-10">
        {!! Form::date("expired_date",null,["class"=>"form-control"]) !!}
    </div>
</div>

