@extends('admin.layout.master')

@section('content')
    <div class="row">
        <div class="col-sm-12"  >
            <div class="card-box">



                <h4 class="header-title m-t-0 m-b-30">Input Types</h4>

    <div class="row">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
            @if (session()->has('success'))
                <div class="alert alert-success">
                        <h4>{{session()->get('success')}}</h4>
                </div>
            @endif

        <div class="col-lg-6">
            {!! Form::open(["route"=>'categories.store' ,'method'=>'post',"files"=>true]) !!}

                <div class="form-group">
                    @include("admin.categories.form")
                    <div class="col-md-10">
                        <button type="submit" class="btn btn-primary">حفظ</button>
                    </div>
                </div>



         {!! Form::close() !!}
        </div><!-- end col -->



    </div><!-- end row -->
    </div>
    </div><!-- end col -->
    </div>
    <!-- end row -->


@endsection
