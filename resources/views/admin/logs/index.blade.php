@extends('admin.layout.master')

@section('styles')
@endsection


@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">
                <div class="dropdown pull-right">
                    <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                        <i class="zmdi zmdi-more-vert"></i>
                    </a>
                    <ul class="dropdown-menu" role="menu">

                    </ul>
                </div>

                <h4 class="header-title m-t-0 m-b-30">Buttons Example</h4>

                <table id="datatable-buttons" class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        @if(app()->getLocale()=="en")

                        <th>{{__("app.name_en")}}</th>
                            @else
                            <th>{{__("app.name_ar")}}</th>
                        @endif

                        <th>{{__("app.user_name")}}</th>
                        <th>{{__("app.date")}}</th>

                    </tr>
                    </thead>

                    <tbody>
                    @forelse($logs as $log)
                        <tr>

                           <td> {{$log->name()}}</td>
{{--                            @dd($log)--}}
{{--                            @foreach($log->users as $user)--}}
                            <td>{{$log->user->name}}</td>

{{--                             @endforeach--}}

                            <td>{{$log->created_at->format('d M Y - H:i:s')}} </td>

                        </tr>
                    @empty
                        <tr>
                            <td></td>
                            <td></td>
                            <td> </td>
                            <td> </td>
                            <td></td>
                        </tr>
                    @endforelse

                    </tbody>
                </table>
            </div>
        </div><!-- end col -->
    </div>
    <!-- end row -->

@endsection



@section('scripts')
@endsection
