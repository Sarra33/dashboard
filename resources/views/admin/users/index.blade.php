@extends('admin.layout.master')

@section('styles')
@endsection


@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">
                <div class="dropdown pull-right">
                    <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                        <i class="zmdi zmdi-more-vert"></i>
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                    </ul>
                </div>

                <h4 class="header-title m-t-0 m-b-30">Buttons Example</h4>

                <table id="datatable-buttons" class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>الإسم</th>
                        <th>البريد الإلكتروني</th>
                        <th>الصورة</th>
                        <th>perrmissions</th>
                    </tr>
                    </thead>

                    <tbody>
                    @forelse($users as $user)
                    <tr>
                        <td>{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        <td>
                            <img src="{{getimg($user->image)}}" style="width: 100px; height: 100px; border-radius: 50%;">
                        </td>
                        <td>
                            @foreach($user->permissions as $permission)
                            <td>{{$permission->name}}</td>
                        @endforeach

                        </td>
                        <td>

                            <a href="/dashboard/users/{{$user->id}}/edit" class="btn btn-primary">تعديل</a>
                            <form action="{{ route('users.destroy',$user->id) }}" method="POST">




                                @csrf
                                @method('DELETE')

                                <button type="submit" class="btn btn-danger">Delete</button>
                            </form>

                               {{-- @endif--}}
                        </td>
                    </tr>
                        @empty
                        <tr>
                            <td></td>
                            <td></td>
                            <td> </td>
                            <td> </td>
                            <td></td>
                        </tr>
                    @endforelse

                    </tbody>
                </table>
            </div>
        </div><!-- end col -->
    </div>
    <!-- end row -->

@endsection



@section('scripts')
@endsection
