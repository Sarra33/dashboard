@extends('admin.layout.master')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">



                <h4 class="header-title m-t-0 m-b-30">Input Types</h4>

                <div class="row">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if (session()->has('success'))
                        <div class="alert alert-success">
                            <h4>{{session()->get('success')}}</h4>
                        </div>
                    @endif
                        <div class="col-lg-6">

{{$user->name}}








                            <form class="form-horizontal" role="form" method="post" action="{{route('users.update',$user->id)}}" enctype="multipart/form-data">
                            {{csrf_field()}}
                                @method('PUT')
                        <div class="form-group">
                                <label class="col-md-2 control-label">الإسم</label>
                                <div class="col-md-10">
                                    <input type="text" name="name" class="form-control" value="{{$user->name}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="example-email" >" >البريد الإلكتروني</label>
                                <div class="col-md-10">
                                    <input type="email"  id="example-email" name="email" class="form-control"  value="{{$user->email}}" placeholder="Email">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">النوع</label>
                                <div class="col-md-10">
                                    <select name="role"  value="{{$user->role}}">
                                        <option >admin</option>
                                        <option>user</option>


                                    </select>

                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">الصلاحيات</label>
                                <div class="col-md-10">





                                    @foreach($permission_collec as $permission)

                                    @if(in_array($permission,$per))
                                        <div class="inline field">
                                            <div class="ui checkbox">
                                                <input type="checkbox" name="permission[]"

                                                       checked="checked"
                                                         value="{{$permission}}"
                                                      > {{$permission}}

                                            </div>
                                            @else
                                                <div class="inline field">
                                                    <div class="ui checkbox">
                                                        <input type="checkbox" name="permission[]"


                                                               value="{{$permission}}"
                                                               tabindex="0" >

                                                    </div>
                                            {{$permission}}
                                        </div>
                                            @endif
                                    @endforeach










                            </div>



                            <div class="form-group">

                                <div class="col-md-10">
                                    <button type="submit" class="btn btn-primary">حفظ</button>
                                </div>
                            </div>

                        </form>
                    </div><!-- end col -->



                </div><!-- end row -->
            </div>
        </div><!-- end col -->
    </div>
    <!-- end row -->


@endsection
