<!-- ========== Left Sidebar Start ========== -->

<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">

        <!-- User -->
        <div class="user-box">
            <div class="user-img">
                <img src="assets/images/users/avatar-1.jpg" alt="user-img" title="Mat Helme" class="img-circle img-thumbnail img-responsive">
                <div class="user-status offline"><i class="zmdi zmdi-dot-circle"></i></div>
            </div>
            <h5><a href="#">Mat Helme</a> </h5>
            <ul class="list-inline">
                <li>
                    <a href="#" >
                        <i class="zmdi zmdi-settings"></i>
                    </a>
                </li>

                <li>
                    <a href="#" class="text-custom">
                        <i class="zmdi zmdi-power"></i>
                    </a>
                </li>
            </ul>
        </div>
        <!-- End User -->

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <ul>
                <li class="text-muted menu-title">Navigation</li>

                <li>
                    <a href="index.html" class="waves-effect"><i class="zmdi zmdi-view-dashboard"></i> <span> {{__("app.main")}} </span> </a>
                </li>
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-invert-colors"></i> <span> {{__("app.users")}}</span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                            @if(Illuminate\Support\Facades\Auth::user()->where('id','=',1) !==[])

                                <li><a href="{{route('users.create')}}">{{__("app.add_user")}}</a></li>



                                <li><a href="{{route('users.index')}}">{{__("app.users")}}</a></li>
                     @endif


                    </ul>
                </li>
              <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-invert-colors"></i> <span> {{__("app.categories")}} </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">

                            @if(Illuminate\Support\Facades\Auth::user()->permissions->where('name','=','add_category') !==[])
                                <li><a href="{{route('categories.create')}}">{{__("app.add_category")}}</a></li>
                            @endif

                            @if(Illuminate\Support\Facades\Auth::user()->permissions->where('name','=','show_categories') !==[])
                                <li><a href="{{route('categories.index')}}">{{__("app.categories")}}</a></li>
                            @endif


                    </ul>
                </li>
                <li class="has_sub">
                <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-invert-colors"></i> <span> {{__("app.products")}} </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">

                            @if(Illuminate\Support\Facades\Auth::user()->permissions->where('name','=','add_product') !==[])
                                <li><a href="{{route('products.create')}}">{{__("app.add_products")}}</a></li>
                             @endif
                            @if(Illuminate\Support\Facades\Auth::user()->permissions->where('name','=','show_products') !==[])
                                <li><a href="{{route('products.index')}}">{{__("app.products")}}</a></li>
                            @endif


                    </ul>



                </li>
                <li class="has_sub">
                <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-invert-colors"></i> <span> {{__("app.logs")}} </span> <span class="menu-arrow"></span></a>
                <ul class="list-unstyled">
                    <li><a href="{{route('logs.index')}}">{{__("app.logs")}}</a></li>

                </ul>


</li>
                </ul>
            <div class="clearfix"></div>
        </div>
        <!-- Sidebar -->
        <div class="clearfix"></div>

    </div>

</div>
<!-- Left Sidebar End -->

--}}
