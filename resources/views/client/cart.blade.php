
@extends('client.layout')

@section('title', 'Cart')

@section('content')

    <table id="cart" class="table table-hover table-condensed">
        <thead>
        <tr>
            <th style="width:50%">Product</th>
            <th style="width:10%">Price</th>
            <th style="width:8%">Quantity</th>
            <th style="width:22%" class="text-center">Subtotal</th>
            <th style="width:10%"></th>
        </tr>
        </thead>
        <tbody>

        <?php $total = 0 ?>

        @if(session('cart'))
            @forelse(session('cart') as $id => $details)

                {{$total += $details['price'] * $details['quantity'] }}

                <tr>

                    <td data-th="Product">
                        <div class="row">
                            <div class="col-sm-3 hidden-xs"><img src="/images/{{ $details['photo'] }}" width="100" height="100" class="img-responsive"/></div>
                            <div class="col-sm-9">
                                <h4 class="nomargin">{{ $details['name'] }}</h4>
                            </div>
                        </div>
                    </td>
                    <td data-th="Price">${{ $details['price'] }}</td>
                      {!!Form::open( ['route' =>['carts.update',$id] , 'method' => 'PATCH']) !!}
                    <td data-th="Quantity">
                        <input  name="quantity" type="number" value="{{ $details['quantity'] }}" class="form-control quantity" />
                    </td>
                    <td data-th="Subtotal" class="text-center">${{ $details['price'] * $details['quantity'] }}</td>
                  <td>  <button class="btn btn-primary" >

                      </button> </td>
                    {!! Form::close() !!}

                    <td class="actions" data-th="">

                        {!!Form::open( ['route' =>['carts.destroy',$id] , 'method' => 'DELETE']) !!}

                        <button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#Delete">
                            <i class="fa fa-trash-o" aria-hidden="true" ></i>
                        </button>
                        {!! Form::close() !!}
                    </td>


                       </td>
                </tr>

                @empty

                <td></td>
                <td></td>
                <td></td>
            @endforelse



        @endif

        </tbody>
        <tfoot>
        <tr class="visible-xs">
            <td class="text-center"><strong>Total {{ $total }}</strong></td>
        </tr>
        <tr>

            {!! Form::open(["route"=>"payment","method"=>"GET"]) !!}
            <td><button class="btn btn-warning"><i class="fa fa-angle-left"></i> Continue Shopping</button>/td>
            {!! Form::close() !!}
            <td colspan="2" class="hidden-xs"></td>
            <td class="hidden-xs text-center"><strong>Total ${{ $total }}</strong></td>
        </tr>
        </tfoot>
    </table>

@endsection