<?php


return[

    "categories"=>"الاقسام",
    "Add_category"=>"اضافه قسم",
    "products"=>"المنتجات",
    "add_product"=>"اضافه منتج",
    "users"=>"المستخدمين",
    "add_user"=>"اضافه مستخدم",
    "main"=>"الرئيسيه",
    "category_name"=>"اسم القسم",
    "category_image"=>"صوره القسم",
    "product_name"=>"اسم المنتج",
"product_price"=>"سعر المنتج",
"product_des"=>"وصف المنتج",
"product_image"=>"صوره المنتج",
"product_cat"=>"القسم",
    "save"=>'حفظ',
    "name_ar"=>"الاسم بالعري",
    "name_en"=>"الاسم بالانجليزي",
    "image"=>"الصوره",
    "user_name"=>"اسم المستخدم"
,"date"=>"التاريخ",
    "logs"=>"عمليات المستخدم",

    "edit"=>"تعديل",
    "delete"=>"حذف",
    "products_no"=>"عدد المنتجات"
];
