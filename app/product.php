<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class product extends Model
{
    use SoftDeletes;
    protected $table = 'products';
    public $timestamps = true;
    protected $fillable = array('name_ar',"name_en" ,'category_id', 'description_en', 'price','image',"expired_date");

    public function category(){

        return $this->belongsTo(\App\Category::class);

    }

  public function name(){
       if(app()->getLocale() == 'ar'){
            return $this->name_ar;
        }else{
            return $this->name_en;
        }
    }


    public function tags(){


        return $this->belongsToMany(tag::class);
    }

    public function discount(){

        return $this->belongsTo(discount::class)
;    }

}
