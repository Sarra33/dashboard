<?php

namespace App\Console\Commands;

use App\product;
use Illuminate\Console\Command;

class delete_expired extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'product:expired';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      product::where( 'expired_date', '=', Carbon::now()->subDays(7))->delete();
    }
}
