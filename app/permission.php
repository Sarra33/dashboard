<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class permission extends Model
{
    protected $fillable = array("name", "role_id");

    public function users()
    {

        return $this->belongsToMany(User::class);
    }


}
