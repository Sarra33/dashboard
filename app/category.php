<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class category extends Model
{
    use SoftDeletes;
//    protected $table ='categories';
    public $timestamps = true;

    protected $fillable = array('name_en',"name_ar","image",'expired_date');



    public function products()
    {
        return $this->hasMany(product::class);
    }


    public function name(){
        if(app()->getLocale() == 'ar'){
            return $this->name_ar;
        }else{
            return $this->name_en;
        }
    }
}
