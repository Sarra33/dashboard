<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class role extends Model
{
    protected $fillable = array('name');

    public  function permission(){

        return $this->hasone(permission::class);
    }

    public function users()


{
        return $this->hasMany(User::class);
    }
}
