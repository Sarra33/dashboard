<?php

namespace App\Traits;

use App\role;
use App\User;
trait HasPermission
{



    public function roles() {
        return $this->belongsTo(role::class);

    }


    public function permissions() {
        return $this->belongsTo(Permission::class);

    }
    public function hasRole( ... $roles ) {
        foreach ($roles as $role) {
            if ($this->roles->contains('name', $role)) {
                return true;
            }
        }
        return false;
    }
    public function hasPermissionThroughRole($permission) {
        foreach ($permission->roles as $role){
            if($this->roles->contains($role)) {
                return true;
            }
        }
        return false;
    }
    public function hasPermissionTo($permission) {
        return $this->hasPermissionThroughRole($permission) || $this->hasPermission($permission);
    }

}