<?php

namespace App;
use App\Http\User;
use Illuminate\Database\Eloquent\Model;

class log extends Model
{
    protected $fillable=array("name_ar","name_en","user_id");

//    public function logs(){
//
//        return $this->belongsToMany(\App\User::class, 'log_user','log_id','user_id');
//    }


public function user(){

    return $this->belongsTo(\App\User::class,"user_id");
}



    public function name(){

        if(app()->getLocale() == 'ar'){
            return $this->name_ar;
        }else{
            return $this->name_en;
        }
    }
}
