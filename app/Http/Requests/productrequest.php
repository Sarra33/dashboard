<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class productrequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //"name_en"=>"required",
           // "name_ar"=>"required|string",
            "category_id" =>"required"
            ,"price"=>"required|int",
            "description_en"=>'required|string',
            "expired_date"=>"required"

        ];
    }
}
