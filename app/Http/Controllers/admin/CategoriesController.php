<?php

namespace App\Http\Controllers\admin;
use App\category;
use App\Http\Controllers\Controller;
use App\log;
use App\product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class CategoriesController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
public function index(){
    if(User_Permisions("show_categories"))
    {$categories=category::all();
//return response($categories);
        return view('admin.categories.index',compact('categories'));}
else
    return response('YOU ARE NOT ALLOWED TI VIEW THIS PAGE :(');

}

/**
 * Show the form for creating a new resource.
 *
 * @return \Illuminate\Http\Response
 */
public function create()
{

 /* $permisions= User_Permisions();
 // return response(($permisions));*/

  if(User_Permisions("add_category")  )
    {

        return view("admin.categories.create");}



else
    return response('YOU ARE NOT ALLOWED TI VIEW THIS PAGE :(');}


/**
 * Store a newly created resource in storage.
 *
 * @param  \Illuminate\Http\Request  $request
 * @return \Illuminate\Http\Response
 */
public function store(Request $request)
{

    $this->validate($request,[
    "name_en"=>"required",
        "name_ar"=>"required"
]);
    $input=$request->all();
    if($file=$request->file("image"))

    {   $name=time().$file->GetClientOriginalName();
        $file->move("images",$name);
        // $image=product_image::create(["file"=>$name,"product_id"=>"$product->id"]);
        $input['image']=$name;




    }

  $category=  category::create($input);

   add_log("created_category","اضافه قسم");


}

/**
 * Display the specified resource.
 *
 * @param  int  $id
 * @return \Illuminate\Http\Response
 */
public function show($id)
{
    //
}

/**
 * Show the form for editing the specified resource.
 *
 * @param  int  $id
 * @return \Illuminate\Http\Response
 */
public function edit($id)
{   // if(User_Permisions("ed"))
    $category=category::find($id);
    return view("admin.categories.update",compact('id','category'));
}

/**
 * Update the specified resource in storage.
 *
 * @param  \Illuminate\Http\Request  $request
 * @param  int  $id
 * @return \Illuminate\Http\Response
 */
public function update(Request $request, $id)
{
    $cat=Categories::find($id);
    $this->validate($request,[
        "name"=>"required"
    ]);
    $input=$request->all();

        if($file=$request->file("image"))

        { $name=time().$file->GetClientOriginalName();
            $file->move("images",$name);
            // $image=product_image::create(["file"=>$name,"product_id"=>"$product->id"]);
            $input['image']=$name;}


        add_log("update_category"," تحديث القسم");
        $categories=category::all();
    return view("admin.categories.index",compact("categories"));
}

/**
 * Remove the specified resource from storage.
 *
 * @param  int  $id
 * @return \Illuminate\Http\Response
 */
public function destroy($id)
{
    add_log("delet_category","حذف قسم");
  $catgory=  Categories::destroy($id);
    unlink(public_path("/images/").$catgory->image);


    return redirect("/dashboard/categories");

}
public function products($id){

    $category=category::find($id);
    $products=$category->products;
// echo "rrrrrrrrrr";
       return  view("admin.products.index")->with("products",$products);



}


}
