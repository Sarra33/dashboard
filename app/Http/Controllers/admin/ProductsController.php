<?php

namespace App\Http\Controllers\admin;
use App\discount;
use App\Http\Requests\productrequest;
use App\permission;
use App\product;
use App\tag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\category;
class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {



           // return response("hi");
       /* $request_name= \Request::route()->getName();


        if( $request_name=="products")

        {  $products = product::all();



            return view ('client.products',compact("products"));

        }*/
     if (User_Permisions("show_product")) {
            $products = product::all();


            return view('admin.products.index', compact(['products', 'contacts']));
        }


else
    return response('YOU ARE NOT ALLOWED TI VIEW THIS PAGE :(');

}



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   if(User_Permisions("add_product"))
    {  if(app()->getLocale()=="ar")
        $category=Category::pluck('name_ar','id')->toArray();
    else
        $category=Category::pluck('name_en','id')->toArray();
        // return response($category);
        return view("admin.products.create",compact("category"));}


        else
            return response('YOU ARE NOT ALLOWED TI VIEW THIS PAGE :(');}





    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(productrequest $request)


    {// return response($request);


        $input=$request->all();

        if($file=$request->file("image"))

        {   $name=time().$file->GetClientOriginalName();
            $file->move("images",$name);
            // $image=product_image::create(["file"=>$name,"product_id"=>"$product->id"]);
            $input['image']=$name;




        }

        /* $discount=discount::create(["name"=>$input["discount_name"],

             'value'=>$input['discount_value']]);


         $input=$input->combine('disount_id',$discount->id);*/
   // $product=product::create(["name_en"=>$input['name_en'] ,'category_id'=>$input['catego'], 'description_en', 'price','image'])
      /* unset($input['discount_value']);
       unset($input['discount_name']);*/

    $product=product::create($input);


  add_log(" create product","اضافه منتج");
return back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Respons
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        $product=product::find($id);
        if(app()->getLocale()=="ar")
            $category=Category::pluck('name_ar','id')->toArray();
        else
            $category=Category::pluck('name_en','id')->toArray();
        // Session::push("previous product","$product");
        return view("admin.products.update",compact("product"),compact("category"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(productrequest $request, $id)
    {
        $input=$request->all();

        unset($input['_method']);
        unset($input['_token']);
        if($file=$request->file("image"))

        { $name=time().$file->GetClientOriginalName();
        $file->move("images",$name);
        // $image=product_image::create(["file"=>$name,"product_id"=>"$product->id"]);
        $input['image']=$name;




    }
        product::find($id)->update($input);
        add_log("update_product" ,"تحديث المنتج");
        return back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {  add_log("delete_product","حذف المنتج");
        $product=product::destroy($id);
        unlink(public_path($product->image));

        return back();
    }


    public function filter(Request $request)
    {
   $filterd_products=product::whereBetween('expired_date', [$request["from"], $request["to"]])->get();
  // echo"ttttttttt";
       //  return response($filterd_products);
return view("admin.products.index")->with("products",$filterd_products);

    }

    public function Tags_Create(){
        $products=product::pluck('name_en','id')->toArray();
        return view("admin.products.ProductTags",compact('products')) ;

    }

   public function Tags_store(Request $request){

        $inputs=$request->all();
        $product=product::find($inputs['product_id']);

  unset($inputs['[product_id']);
       foreach ($inputs['tags'] as $value )
       {


           $tag=  tag::where(["name"=>$value])->get();

           $product->tags()->attach($tag);
       }





   }

   public function DiscountCreate(){


       $products=product::pluck('name_en','id')->toArray();
        return view('admin.products.ProductDiscount',compact('products'));

   }

public function DiscountStore( Request $request)
{  $input=$request->all();

     $product_id=$input['product_id'];
    unset($input['product_id']);
    $discount=discount::create($input);
   // return($discount->id);
    $product=product::find($product_id);
    $product['discount_id']=$discount->id;
    $product->save();
        //->update(["discount_id"=>$discount->id]);
 // return  response($product->discount_id);


}



}
