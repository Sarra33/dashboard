<?php
return [
    'client_id' => env('PAYPAL_CLIENT_ID',''),
    'secret' => env('PAYPAL_SECRET',''),
    'settings' => array(
        'mode' => env('PAYPAL_MODE','sandbox'),
        'http.ConnectionTimeOut' => 30,
        'log.LogEnabled' => true,
        'log.FileName' => storage_path() . '/logs/paypal.log',
      //  'log.FileName' => 'vendor\paypal\rest-api-sdk-php\lib\PayPal\Log\PayPalDefaultLogFactory.php',
        'log.LogLevel' => 'ERROR'
    ),
];